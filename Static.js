var Static = {
	version: "0.3.0",
	debug: 0
};

function createHash(str) {
	var hash = 0, i, chr;
	for (i = 0; i < str.length; i++) {
	  chr   = str.charCodeAt(i);
	  hash  = ((hash << 5) - hash) + chr;
	  hash |= 0; // Convert to 32bit integer
	}
	return hash;
}
function each(elements, callbackFN) {
	if ( isArrayLike(elements) ) {
		for (var i = 0; i < elements.length; i++) {
			if ( callbackFN(elements[i], i, elements)) {
				break;
			}
		}
	} else if ( elements !== null && typeof elements == "object" && !Array.isArray(elements) && Object.keys(elements).length ) {

		for (key in elements) {
			if (elements.hasOwnProperty(key)) {
				callbackFN( elements[key], key, elements );
			}
		}
	}
}
function map(elements, callbackFN) {
	if (  isArrayLike(elements) ) {
		return Array.prototype.map.call(elements, callbackFN);
	}
	return [];
}
function isIterable(obj) {
	if (obj === null || typeof obj != "object") {
		return false;
	}
	if ( typeof Symbol == "undefined" ) {
		return "length" in obj;
	}
	return typeof obj[Symbol.iterator] === "function";
}
function isArrayLike(item) {
	return (
		Array.isArray(item) ||
		( !!item &&
		  typeof item === "object" &&
		  typeof (item.length) === "number" &&
		  (item.length === 0 ||
			 (item.length > 0 &&
			 (item.length - 1) in item)
		  )
		)
	);
}

function doWhen(callback, conditionalCallback, interval) {

	var privateWrapper = interval ? function (callback) {
		setTimeout(callback, interval);
	} : requestAnimationFrame;

	var onInterval = function () {
		!conditionalCallback() && privateWrapper(onInterval) || callback();
	}
	onInterval();
}
function matches(string, regex) {

	return string.match(regex) || [];
}
function parseJSON(string) {
	var data;
	try{
		data = JSON.parse(string);
	} catch (e) {
		console.error(e);
	}
	return data;
}
function preventDefault(event) {
	event.preventDefault();
}



var http = {};
http.getUrlParams = function() {

	var params = {};
	var query = document.location.search;

	if ( query.length ){

		query = query.replace(/^\?/,"");
		query = query.replace(/#.*/,"");
		var keyValues = query.split("&");

		for (var i = 0, keyValue; i < keyValues.length; i++) {

			keyValue = keyValues[i].split("=");

			if ( keyValue.length == 2 ){

				params[ decodeURIComponent(keyValue[0]) ] = decodeURIComponent(keyValue[1]);
			}
		}
	}
	return params;
}
http.request = function (url, onSuccess, onError, options) {

	var xhr = new XMLHttpRequest();

	options = options || {};

	options.method = options.method || "GET";
	xhr.open( options.method, url );

	for (var prop in options) {
		if (options.hasOwnProperty(prop)) {
			xhr[prop] = options[prop];
		}
	}

	xhr.onload = function () {
		if ( Static.debug ) console.log('Static.requestUrl.onload', xhr);

		var response = xhr.response;

		if ( xhr.responseType == "json" && typeof response != "object" ) {
			if ( Static.debug ) console.error( "Static.request not getting json...")
			try{
				response = JSON.parse(xhr.responseText);
			} catch(e){
				if ( Static.debug ) console.error( "Static.request error parsing json", e)
			}
		}
		onSuccess(response);
	}
	xhr.onerror = function () {
		if ( Static.debug ) console.log('Static.requestUrl.onerror', xhr);
		onError(xhr);
	}
	xhr.send( options.data );
}
http.isSameHost = function isSameHost(href1, href2) {

	var host1, host2;

	if ( typeof href1 == "string" ) {
		var el = document.createElement("a");
			el.setAttribute("href", href1);
		host1 = el.host;
	} else if ( typeof href1 == "object" && href1 !== null && href1.host ) {
		host1 = href1.host;
	}

	if ( typeof href2 == "string" ) {
		var el = document.createElement("a");
			el.setAttribute("href", href2);
		host2 = el.host;
	} else if ( typeof href2 == "object" && href2 !== null && href2.host ) {
		host2 = href2.host;
	}

	if ( host1 && host2 ) {
		return host1 === host2;
	}
}
http.isSamePath = function(href1, href2) {

	href1 = href1.replace(/#.*$/, "").replace(/\/$/, ""); // remove hash and trailing slash
	href2 = href2.replace(/#.*$/, "").replace(/\/$/, "");

	if ( href1.length > href2.length ) {

		if (href1.lastIndexOf(href2) != (href1.length - href2.length)) {

			return false;
		}
	} else if (href2.lastIndexOf(href1) != (href2.length - href1.length)) {

		return false;
	}
	return true;
}
http.requestDom = function (url, onSuccess, onError, options) {

	var xhr = new XMLHttpRequest();

	options = options || {};
	options.method = options.method || "GET";

	xhr.open( options.method, url );
	xhr.responseType = "document";
	xhr.onload = function () {
		if ( Static.debug ) console.log('Static.requestUrl.onload', xhr);
		onSuccess(xhr.response);
	}
	xhr.onerror = function () {
		if ( Static.debug ) console.log('Static.requestUrl.onerror', xhr);
		onError(xhr);
	}
	xhr.send( options.data );
}

var dom = {};
dom._ready = function () {
	var r = document.readyState;
	return (r == "complete" || r == "interactive");
}
dom.ready = function (callback) {
	if ( dom._ready() ) {
		callback();
	} else {
		var e;
		e = function () {
			if ( dom._ready() ) {
				callback();
				document.removeEventListener("readystatechange", e);
			}
		}
		document.addEventListener("readystatechange", e);
	}
}
dom.style = function (element, properties, value) {
	if ( typeof properties == "object" ) {
		for (prop in properties) {
			if (properties.hasOwnProperty(prop)) {
				element.style[prop] = properties[prop];
			}
		}
	} else if ( typeof properties == "string" && value ) {
		element.style[properties] = value;
	}
}
dom.swapChildren = function(el1, el2) {

	if ( el1.parentNode != el2.parentNode ) return false;

	var parent = el1.parentNode;
	var i1, i2;

	for (var i = 0; i < parent.children.length; i++) {

		if (parent.children[i].isEqualNode(el1)) {
			i1 = i;
		} else if (parent.children[i].isEqualNode(el2)) {
			i2 = i;
		}
	}

	parent.insertBefore(el1, el2);

	if (i1 < i2){

		parent.insertBefore(el2, parent.children[i1 - 1]);
		parent.insertBefore(document.createTextNode(" "), parent.children[i1 - 1]);
	} else {

		parent.insertBefore(el2, parent.children[i1 + 1]);
		parent.insertBefore(document.createTextNode(" "), parent.children[i1 + 1]);
	}
}
dom.insertAt = function ( element, parent, index ) {

	var referenceElementAfter = parent.children[index - 1];
	if ( referenceElementAfter ) {
		if ( element == referenceElementAfter ) {
			return;
		}
		return dom.insertAfter( element, parent.children[index - 1])
	}

	var referenceElementBefore = parent.children[index];
	if ( referenceElementBefore ) {
		if ( element == referenceElementBefore ) {
			return;
		}
		return dom.insertBefore( element, parent.children[0] )
	}

	parent.appendChild( element );
}
dom.insertAfter = function ( element, referenceElement ) {
	if ( referenceElement.nextElementSibling ) {
		return dom.insertBefore( element, referenceElement.nextElementSibling );
	} else {
		var p = referenceElement.parentNode;
		if ( p ) {
			return p.appendChild( element );
		}
	}
}
dom.insertBefore = function ( element, referenceElement ) {
	var p = referenceElement.parentNode;
	if ( p ) {
		return p.insertBefore( element, referenceElement );
	}
}
dom.removeNode = function ( node ) {

	var parent = node.parentNode;
	if ( parent ) {
		parent.removeChild( node );
	}
}
dom.closest = function (el, selector) {

	var closest;
	var parent = el.parentNode;
	while ( parent && !(closest = queryParent(parent, selector)) ){
		parent = parent.parentNode;
	}

	function queryParent(parent, selector) {
		var all = parent.querySelectorAll(selector);
		all = Array.prototype.filter.call(all, isElementParentOrElement);
		return all.length > 0 && all[0];
	}

	function isElementParentOrElement(parent) {
		var child = el;
		while ( child && child.parentNode != parent && child != parent ) {
			child = child.parentNode;
		}
		return child;
	}

	return closest;
}
dom.indexOf = function (child, nodeList) {

	for (var i = 0; i < nodeList.length; i++) {
		if ( child == nodeList[i] ) return i;
	}
	return -1;
}
dom.closestOf = function (el, elements) {

	while ( el != null ) {

		if ( dom.contains(elements, el) ) {
			return el;
		}
		el = el.parentNode;
	}
}
dom.contains = function(nodeList, node){

	if ( typeof nodeList.contains == "function" ) {

		return nodeList.contains( node );
	}
	if ( !isNaN(nodeList.length) ) {

		for (var i = 0; i < nodeList.length; i++) {
			if ( node == nodeList[i] ) return true;
		}
	}
	return false;
}

dom.listeners = [];
// features:
// once parameter
// resizeend even
dom.attachListener = function (element, type, callback, capture, once) {

	var callbackWrapper = function (event) {
		if ( once ) {
			element.removeEventListener( type, callbackWrapper, capture );
		}
		callback.call(event.target, event);
	}
	if ( type == "resizeend" && typeof window._onWindowResize === "undefined" ) {
		var timeout = 0;
		window._onWindowResize = function (event) {
			if (!dom._resizeStart) dom._resizeStart = window.innerWidth;
			clearTimeout(timeout);
			timeout = setTimeout(dom._onResizeEnd.bind(window, event), dom._onResizeEndDelay)
		}
		dom.attachListener(window, "resize", window._onWindowResize);
	}
	dom.listeners.push({
		element: element,
		type: type,
		callback: callback,
		callbackWrapper: callbackWrapper,
		capture: capture
	});
	element.addEventListener( type, callbackWrapper, capture );
}
dom.detachListener = function (element, type, callback, capture) {

	dom.listeners = (dom.listeners || []).map(function (listener) {

		var remove = true;

		if ( element && element !== listener.element ) {
			remove = false;
		}

		if ( type && type !== listener.type ) {
			remove = false;
		}

		if ( callback && callback !== listener.callback ) {
			remove = false;
		}

		if ( remove ) {
			listener.element.removeEventListener( listener.type, listener.callbackWrapper, listener.capture );
		}
		return remove;
	});
}
dom._resizeStart = null;
dom.onResizeEnd = function (callback) {
	if ( Static.debug ) console.warn("dom.onResizeEnd is deprecated, use dom.attachListener");
	dom.attachListener( "resizeend", callback );
}
dom._onResizeEndDelay = 250;
dom._onResizeEnd = function (e) {
	e.data = {start: dom._resizeStart};
	dom._resizeStart = null;
	each(dom.listeners, function (listener) {
		if ( listener.type == "resizeend" ) {
			listener.callback(e, listener);
		}
	});
}
dom.createEvent = function ( properties ) {
	var event = null;
	var type = (typeof properties == "object" ? properties.type : properties )
	if ( type ) {
		if ( typeof(Event) === "function" ) {
			event = new Event(type);
		} else {
			event = document.createEvent("Event");
			event.initEvent(type, true, true);
		}
		if ( typeof properties == "object" ) {
			for (prop in properties) {
				if (properties.hasOwnProperty(prop)) {
					if ( prop != "type" ) {
						Object.defineProperty(event, prop, {value: properties[prop], enumerable: true});
					}
				}
			}
		}
	}
	return event;
}
dom.loadStyle = function(url) {
	return new Promise(function (resolve, reject) {
		var element = document.createElement("link");
			element.setAttribute("type", "text/css");
			element.setAttribute("rel", "stylesheet");
			element.onload = function () {
				if ( Static.debug ) {
					console.log("Static.loadStyle loaded", url);
				}
				resolve(url);
			};
			element.onerror = function () {
				reject(url);
			};
		document.head.appendChild(element);
		element.setAttribute("href", url);
	});
}
dom.loadScript = function(url) {
	return new Promise(function (resolve, reject) {
		var element = document.createElement("script");
			element.setAttribute("async", "");
			element.onload = function () {
				if ( Static.debug ) {
					console.log("Static.loadScript loaded", url);
				}
				resolve(url);
			};
			element.onerror = function () {
				reject(url);
			};
		document.body.appendChild(element);
		element.setAttribute("src", url);
	});
}
function loadScript(){
	console.log("deprecated, use Static.dom.loadScript");
	dom.loadScript.apply(arguments);
}
function loadStyle(){
	console.log("deprecated, use Static.dom.loadStyle");
	dom.loadStyle.apply(arguments);
}

Static.dom = dom;
Static.http = http;

Static.createHash = createHash;
Static.each = each;
Static.map = map;
Static.isIterable = isIterable;
Static.isArrayLike = isArrayLike;

Static.doWhen = doWhen;
Static.matches = matches;
Static.parseJSON = parseJSON;
Static.preventDefault = preventDefault;

if ( typeof module == "object" ) {
	module.exports = Static;
}

export default Static;
export {dom, http, createHash, each, map, isIterable, isArrayLike, doWhen, matches, parseJSON, preventDefault};